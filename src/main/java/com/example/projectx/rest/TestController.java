package com.example.projectx.rest;



@org.springframework.web.bind.annotation.RestController
@org.springframework.web.bind.annotation.RequestMapping("/test")
@lombok.extern.slf4j.Slf4j
public class TestController {

    @org.springframework.web.bind.annotation.GetMapping
    public String test() {
        return "Test return";
    }
}
